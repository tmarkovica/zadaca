﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV6_Igra_vjesala.Properties
{
    class Rijec
    {
        #region data_members
        private string rijec;
        #endregion

        #region public_methods

        public Rijec()
        {
        }

        public Rijec(string str)
        {
            rijec = str;
        }

        public string dajRijec
        {
            get
            {
                return rijec;
            }
            set
            {
                rijec = value;
            }
        }
        

        public override string ToString()
        {
            return "";
        }

        #endregion
    }
}
