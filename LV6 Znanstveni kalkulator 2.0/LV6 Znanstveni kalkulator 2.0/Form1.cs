﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV6_Znanstveni_kalkulator_2._0
{
    public partial class ZnanstveniKalkulator : Form
    {
        public ZnanstveniKalkulator()
        {
            InitializeComponent();
        }

        private double rezultat;
        private double prviBroj;
        private int br = 0;
        private string operand;
        private bool greska = false;

        private void UnosBroja(object sender, EventArgs e)
        {
            Button Broj = (Button)sender;
            if(br<=12)
            {
                if (display.Text == "0")
                    display.Text = Broj.Text;
                else
                    display.Text += Broj.Text;

                if(double.TryParse(display.Text, out prviBroj))
                    br++;
            }
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            display.Text = "0";
            display2.Text = "";
            rezultat = 0;
            br = 0;
            if(greska)
                Button_on_of();
            greska = false;
        }

        private void buttonCE_Click(object sender, EventArgs e)
        {
            if(display.Text != "0")
            {
                display.Text = display.Text.Remove(display.Text.Length - 1);
                if(br > 0)
                    br--;
            }

            if(display.Text == "")
                display.Text = "0";

            if (!double.TryParse(display.Text, out prviBroj))
                prviBroj = 0;
        }

        private void Operacija(object sender, EventArgs e)
        {
            Button Operand = (Button)sender;

            string operandPrev = operand; //***
            operand = Operand.Text;

            if (display.Text == "0")
            {
                prviBroj = 0;
            }

            if (display2.Text != "")
            {
                if (operandPrev == operand)
                {
                    Izracun();
                }
                else
                {
                    string temp = operand;
                    operand = operandPrev;
                    operandPrev = temp;

                    Izracun();

                    operand = operandPrev;
                }
            }
            else
            {
                if(double.TryParse(display.Text, out prviBroj))
                    rezultat = prviBroj;
            }
            br = 0;
            display2.Text = rezultat.ToString();
            display2.Text += " " + operand;
            display.Text = "0";
        }

        private void Izracun()
        {
            if (operand == "+")
                rezultat += prviBroj;
            if (operand == "-")
                rezultat -= prviBroj;
            if (operand == "*")
                rezultat *= prviBroj;
            if (operand == "/")
                if (prviBroj != 0)
                    rezultat /= prviBroj;
                else
                    Greska();
        }

        private void buttonJednako_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(display.Text, out prviBroj))
                prviBroj = 0;

            if(display2.Text != "")
            {
                Izracun();
            }
            else
            {
                rezultat = prviBroj;
            }

            display2.Text = "";
            if(!greska)
                display.Text = rezultat.ToString();
        }

        private void Greska()
        {
            display.Text = "Error";
            greska = true;
            Button_on_of();
        }

        private void Button_on_of()
        {
            button1.Enabled = !button1.Enabled;
            button2.Enabled = !button2.Enabled;
            button3.Enabled = !button3.Enabled;
            button4.Enabled = !button4.Enabled;
            button5.Enabled = !button5.Enabled;
            button6.Enabled = !button6.Enabled;
            button7.Enabled = !button7.Enabled;
            button8.Enabled = !button8.Enabled;
            button9.Enabled = !button9.Enabled;
            button10.Enabled = !button10.Enabled;
            button11.Enabled = !button11.Enabled;
            buttonPlus.Enabled = !buttonPlus.Enabled;
            buttonMinus.Enabled = !buttonMinus.Enabled;
            buttonPuta.Enabled = !buttonPuta.Enabled;
            buttonPodijeljeno.Enabled = !buttonPodijeljeno.Enabled;
            buttonJednako.Enabled = !buttonJednako.Enabled;
            buttonCE.Enabled = !buttonCE.Enabled;
            buttonSinus.Enabled = !buttonSinus.Enabled;
            buttonCosinus.Enabled = !buttonCosinus.Enabled;
            buttonSqrt.Enabled = !buttonSqrt.Enabled;
            buttonSqr.Enabled = !buttonSqr.Enabled;
            buttonLog.Enabled = !buttonLog.Enabled;
        }

        private void buttonSqrt_Click(object sender, EventArgs e)
        {
            if (double.TryParse(display.Text, out prviBroj))
                prviBroj = Math.Sqrt(prviBroj);

            if (display2.Text != "")
            {
                Izracun();
            }
            else
            {
                rezultat = prviBroj;
            }
            display.Text = rezultat.ToString();
            display2.Text = "";
        }

        private void buttonSinus_Click(object sender, EventArgs e)
        {
            if (double.TryParse(display.Text, out prviBroj))
                prviBroj = Math.Sin(prviBroj);
            if (display2.Text != "")
            {
                Izracun();
            }
            else
            {
                rezultat = prviBroj;
            }
            display.Text = rezultat.ToString();
            display2.Text = "";
        }

        private void buttonCosinus_Click(object sender, EventArgs e)
        {
            if (double.TryParse(display.Text, out prviBroj))
                prviBroj = Math.Cos(prviBroj);

            if (display2.Text != "")
            {
                Izracun();
            }
            else
            {
                rezultat = prviBroj;
            }
            display.Text = rezultat.ToString();
            display2.Text = "";
        }

        private void buttonSqr_Click(object sender, EventArgs e)
        {
            if (double.TryParse(display.Text, out prviBroj))
                prviBroj *= prviBroj;

            if (display2.Text != "")
            {
                Izracun();
            }
            else
            {
                rezultat = prviBroj;
            }
            display.Text = rezultat.ToString();
            display2.Text = "";
        }

        private void buttonLog_Click(object sender, EventArgs e)
        {
            if (double.TryParse(display.Text, out prviBroj))
            { 
                if(prviBroj > 0)
                {
                    prviBroj = Math.Log10(prviBroj);
                    if (display2.Text != "")
                    {
                        Izracun();
                    }
                    else
                    {
                        rezultat = prviBroj;
                    }
                    display.Text = rezultat.ToString();
                    display2.Text = "";
                }
                else
                {
                    Greska();
                }
            }
        }
    }
}
