﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//????
using LV6_Igra_vjesala.Properties;
//using LV6_Igra_vjesala;

namespace LV6_Igra_vjesala
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            string adresa = "C:/Users/Tomislav/source/repos/LV6 Igra vjesala/LV6 Igra vjesala/Rijecnik.txt";

            int brRijeci = 0;

            string[] lines = System.IO.File.ReadAllLines(adresa);

            foreach (string line in lines)
            {
                Rijec r = new Rijec(line);
                listaRijeci.Add(r);
                brRijeci++;
            }

            // random number generator!!!!
            Random rnd = new Random();
            randIndeks = rnd.Next(0, brRijeci);

            for (int i=0; i < listaRijeci[randIndeks].dajRijec.Length; i++)
            {
                textBox1.Text += "_ ";

                prikazRijeci += listaRijeci[randIndeks].dajRijec.Substring(i, 1) + " ";
            }
            labelPokusaji.Text = brPokusaja.ToString();
        }

        private List<Rijec> listaRijeci = new List<Rijec>();
        private int randIndeks = 0;
        private string prikazRijeci = "";
        private int brPokusaja = 15;

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != "" && brPokusaja > 0)
            {
                if (listaRijeci[randIndeks].dajRijec.ToLower().Contains(textBox2.Text)
                    || listaRijeci[randIndeks].dajRijec.ToUpper().Contains(textBox2.Text))
                {
                    char cLow, cHigh;
                    

                    string temp = textBox1.Text;
                    textBox1.Text = "";

                    for (int i = 0; i < listaRijeci[randIndeks].dajRijec.Length; i++)
                    {
                        cLow = Convert.ToChar(listaRijeci[randIndeks].dajRijec.ToLower().Substring(i, 1));
                        cHigh = Convert.ToChar(listaRijeci[randIndeks].dajRijec.ToLower().Substring(i, 1));

                        if(cLow.ToString() == textBox2.Text || cHigh.ToString() == textBox2.Text)
                        {
                            textBox1.Text += cLow.ToString() + " ";
                        }
                        else
                        {
                            if (temp.Contains(cLow) || temp.Contains(cHigh))
                            {
                                textBox1.Text += cLow.ToString() + " ";
                            }
                            else
                            {
                                textBox1.Text += "_ ";
                            }
                        }

                        if(textBox1.Text == prikazRijeci)
                        {
                            MessageBox.Show("Pronašli ste riječ.");
                            button1.Enabled = false;
                            buttonPogodiRijec.Enabled = false;
                        }
                    }
                }
                brPokusaja--;
            }
            else if(textBox2.Text != "")
            {
                brPokusaja--;
            }

            labelPokusaji.Text = brPokusaja.ToString();

            if (brPokusaja == 0)
            {
                MessageBox.Show("Nemate vise pokušaja.");
                button1.Enabled = false;
                buttonPogodiRijec.Enabled = false;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if(textBox2.Text != "")
            {
                if (textBox2.Text.Length > 1)
                {
                    //MessageBox.Show("Mozete unijeti samo jedno slovo.\n");
                    textBox2.Text = textBox2.Text.Remove(textBox2.Text.Length - 1);
                }
                else
                {
                    char c = Convert.ToChar(textBox2.Text.Substring(0, 1));

                    if (!char.IsLetter(c))
                    {
                        textBox2.Text = "";
                        //MessageBox.Show("Mozete unijeti samo slova.\n");
                    }
                }
            }
                
        }

        private void buttonNovaIgra_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void buttonPogodiRijec_Click(object sender, EventArgs e)
        {
            if(textBox3.Text.ToLower() == listaRijeci[randIndeks].dajRijec || 
                textBox3.Text.ToUpper() == listaRijeci[randIndeks].dajRijec)
            {
                MessageBox.Show("Bravo, pogodili ste riječ.");
                button1.Enabled = false;
                buttonPogodiRijec.Enabled = false;
                brPokusaja--;
                textBox1.Text = prikazRijeci;
            }
            else
            {
                if(brPokusaja==0)
                {
                    button1.Enabled = false;
                    buttonPogodiRijec.Enabled = false;
                }
                else
                {
                    brPokusaja--;
                }
            }
        }
    }
}
